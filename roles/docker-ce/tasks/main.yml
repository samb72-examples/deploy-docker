   ## Subscribe server to subscription manager. This sometimes fails so a rety is built in.
   ## Username and password variables are in Vault.
   
   - name: Register server with subscription-manager
     shell: subscription-manager register --force --username={{ sub_username }} --password={{ sub_password }}
     register: reg_result
     retries: 5
     delay: 5
     until: reg_result.rc == 0
     no_log: true
     when: hostvars[inventory_hostname]['ansible_distribution'] == 'Red Hat Enterprise Linux'

   - name: Attach server to subscription manager
     shell: subscription-manager attach
     register: attach_result
     retries: 5
     delay: 5
     until: attach_result.rc == 0
     when: hostvars[inventory_hostname]['ansible_distribution'] == 'Red Hat Enterprise Linux'


   ## Check if docker exists on the server. If not then the next few steps are skipped.
   
   - name: Check for existing docker install
     stat:
      path: /usr/bin/docker
     register: docker_exists

   ## Esnure docker is started so that we can remove all existing containers and volumes

   - name: Ensure docker started
     service:
      name: docker
      state: started
     when: docker_exists.stat.exists


   ## Remove all containers and volumes

   - name: Remove all existing docker containers and volumes
     shell: docker stop $(docker ps -q) && docker rm $(docker ps -q -a) && docker rmi $(docker images -q) --force ; docker system prune
     when: docker_exists.stat.exists

   ## Stop docker before removing

   - name: Stop docker
     service:
      name: docker
      state: stopped
     when: docker_exists.stat.exists

   ## Remove old versions of docker
    
   - name: Remove existing version of Docker
     yum:
      name: "{{ item }}"
      state: absent
     with_items: "{{ packages_to_remove }}"
     when: docker_exists.stat.exists

   ## Unlink/remove existing /var/lib/docker folder
   
   - name: Unlink or remove existing /var/lib/docker folder
     file:
      path: /var/lib/docker
      state: absent

   ## Install required packages
  
   - name: Install required packages
     yum:
      name: "{{ item }}"
      state: latest
     with_items: "{{ required_packages }}"


   ## Ensure that rhel-7-server-extras-rpms is enabled

   - name: Ensure rhel-7-server-extras-rpms is enabled
     shell: yum-config-manager --enable rhel-7-server-extras-rpms
     tags:
      - optional


   ## Remove all current docker repos
   
   - name: Get a list of all docker repos
     find:
      paths: /etc/yum/repos.d
      patterns: docker*.repo
     register: docker_repos   


   - name: Remove current docker repos
     file:
      path: "{{ item }}"
      state: absent
     with_items: "{{ docker_repos.files }}"


   ## Add docker-CE Stable repo

   - name: Add docker-CE stable repo
     shell: "yum-config-manager --add-repo 'https://download.docker.com/linux/{{ docker_os }}/docker-ce.repo'"

   ## Install docker-CE

   - name: Install docker-CE
     yum:
      name: docker-{{ docker_type }}-{{ docker_package_name }}
      state: present


   ## Make sure docker is started to create intial configuration folder

   - name: Start docker
     service:
      name: docker
      state: started

   ## Make sure docker is stopped before doing config copy
   
   - name: Stop docker
     service:
      name: docker
      state: stopped
   
   ## Ensure there is a /apps/devops folder.

   - name: Ensure /apps/devops folder exists
     file:
      path: /apps/devops
      state: directory
   
   ## Remove existing /apps/devops/docker folder
  
   - name: Remove existing /apps/devops/docker
     file:
      path: /apps/devops/docker
      state: absent
  
 
   ## Copy docker application files to /apps/devops

   - name: Copy docker application to /apps/devops
     synchronize:
      src: /var/lib/docker
      dest: /apps/devops/
      archive: yes
     delegate_to: "{{ inventory_hostname }}"
  
   # Remove /var/lib/docker folder so we can create symlink
   
   - name: Remove /var/lib/docker folder so we can create symlink 
     file:
      name: /var/lib/docker
      state: absent
   
   # Create symlink /var/lib/docker

   - name: Create symlink for /var/lib/docker
     file:
      src: /apps/devops/docker
      dest: /var/lib/docker
      state: link


   ## Deploy docker daemon template

   - name: Deploy docker daemon template
     template:
      src: ./templates/daemon.json.j2
      dest: /etc/docker/daemon.json
       

   ## Restart docker to take config changes and enable it so it starts on boot
   
   - name: Start Docker and have it restart on reboots
     service:
      name: docker
      state: restarted
      enabled: yes
